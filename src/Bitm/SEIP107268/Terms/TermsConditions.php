<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Bitm\SEIP107268\Terms;

use App\Bitm\SEIP107268\Utility\Utility;

class TermsConditions {

    public $id = "";
    public $term_detail = "";
    public $term_name = "";

//    public $created = "";
//    public $modified = "";
//    public $created_by = "";
//    public $modified_by = "";
//    public $deleted_at = "";

    public function __construct($data = false) {
        if (is_array($data) && array_key_exists('id', $data) && !empty($data['id'])) {
            $this->id = $data['id'];
        }
       @ $this->term_detail = $data['detail'];
        @$this->term_name = $data['terms'];
    }

    public function index() {
        $days = array();

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");

        $query = "SELECT * FROM `terms`";
        $result = mysql_query($query);

        while ($row = mysql_fetch_assoc($result)) {
            $days[] = $row;
        }
        return $days;
    }

    public function create() {
        echo 'This is create data';
    }

    public function show($id = false) {

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");

        $query = "SELECT * FROM `terms` WHERE id =" . $id;
        $result = mysql_query($query);

        $row = mysql_fetch_assoc($result);
        return $row;
    }

    public function edit() {
        echo 'This is edit data';
    }

    public function update() {

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");

        $query = "UPDATE `atomicproject`.`terms` SET `terms_details` = '" . $this->term_detail . "',`terms` = '" . $this->term_name . "' WHERE `terms`.`id` = " . $this->id;
        $result = mysql_query($query);
        if ($result) {
            Utility::message("Book Title is successfully Edited");
        } else {
            Utility::message("Book Title is not inserted, try again later");
        }
        Utility::redirect('index.php');
    }

    public function store() {
        $con = mysql_connect("localhost", "root", "") or die("Can not connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Can not select database");

        $query = "INSERT INTO `atomicproject`.`terms` (`terms_details`, `terms`) VALUES ('" . $this->term_detail . "','" . $this->term_name . "');";
        $result = mysql_query($query);
        if ($result) {
            Utility::message("City Name is successfully Inserted");
        } else {
            Utility::message("City Name is not inserted, try again later");
        }
        Utility::redirect('index.php');
    }

    public function delete($id = null) {
        if (is_null($id)) {
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }

        $con = mysql_connect("localhost", "root", "") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");

        $query = "DELETE FROM `atomicproject`.`terms` WHERE `terms`.`id` = " . $id;

        $result = mysql_query($query);
        if ($result) {
            Utility::message("Terms is successfully Deleted");
        } else {
            Utility::message("terms can not delete");
        }
        Utility::redirect('index.php');
    }

}
