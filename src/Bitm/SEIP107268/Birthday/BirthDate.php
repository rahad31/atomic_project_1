<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Bitm\SEIP107268\Birthday;
use App\Bitm\SEIP107268\Utility\Utility;

class BirthDate {
    public $id="";
    public $date="";
    public $created="";
    public $modified="";
    public $created_by="";
    public $modified_by="";
    public $deleted_at="";
    
    
    public function __construct($data = false){
        if(is_array($data)&& array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->date = $data['date'];
    }
    
    public function index(){
        $days = array();
        $con = mysql_connect("localhost","root","") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");
        
        $query = "SELECT * FROM `birthdays`";
        $result = mysql_query($query);
        
        while($row = mysql_fetch_object($result)){
            $days[] = $row;
        }
        return $days;
    }
    
    public function show($id = false){
        
        $con = mysql_connect("localhost","root","") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");
        
        $query = "SELECT * FROM `birthdays` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        return $row;
    }
    
    
    public function create(){
        echo 'This is create data';
    }
    
    public function edit(){
        echo 'This is edit data';
    }
   
    
    public function update(){
        
        $con = mysql_connect("localhost","root","") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");
        
        $query = "UPDATE `atomicproject`.`birthdays` SET `birthdate` = '".$this->date."' WHERE `birthdays`.`id` = ".$this->id;
        $result = mysql_query($query);
        if($result){
            Utility::message("Birthday is successfully Edited");
        }  else {
            Utility::message("birthday is not Edited, try again later");
        }
        Utility::redirect('index.php');
    }
    
    
    public function store(){
       $con = mysql_connect("localhost","root","") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");
        
        $query = "INSERT INTO `atomicproject`.`birthdays` (`birthdate`) VALUES ('".$this->date."');";
        $result = mysql_query($query);
        if($result){
            Utility::message("Birthday is successfully inserted");
        }  else {
            Utility::message("Birthday is not inserted, try again later");
        }
        Utility::redirect('index.php');
    }
    
    public function delete($id = null){
        if(is_null($id)){
            Utility::Message("No, id available sorry");
            return Utility::redirect("index.php");
        }
        
        $con = mysql_connect("localhost","root","") or die("Cannot Connect to database");
        $lnk = mysql_select_db("atomicproject") or die("Cannot Select database");
        
        $query = "DELETE FROM `atomicproject`.`birthdays` WHERE `birthdays`.`id` = ".$id;
       
        $result = mysql_query($query);
        if($result){
            Utility::message("Birthdate is successfully Deleted");
        }  else {
            Utility::message("Birthdate can not delete");
        }
        Utility::redirect('index.php');
        
    }
}
