<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>City Name</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-info">
        <div class="container">
            
            <h3 class="text-center text-success">Add New City</h3>
            <hr>
            <form class="form-inline" action="store.php" method="POST">
                <div class="form-group text-primary">
                    <label for="exampleInputName2">Enter City Name: </label>
                    <select class="form-control" name="cityname">
                        <option value="Dhaka">Dhaka</option>
                        <option value="Jamalpur">jamalpur</option>
                        <option value="Sylhet">Sylhet</option>
                        <option value="Vola">Vola</option>
                    </select>
                </div>
                
                <hr>
                <button type="submit" class="btn btn-success">Submit</button>
            </form>
            
            
         <?php

        include_once '../../../vendor/autoload.php';

        //function __autoload($className) {
        //    $filename = '../../../' . str_replace("\\", "/", $className) . '.php';
            //echo $filename;
         //   include_once($filename);
        //}

        use App\Bitm\SEIP107268\City\CityName;

        $city= new CityName();
        $city->create();
        ?>
            
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
        
    </body>
</html>