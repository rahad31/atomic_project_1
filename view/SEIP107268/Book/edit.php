<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Book Title</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'atomicproject-rahad-107268' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

        use App\Bitm\SEIP107268\Book\BookTitle;
        use App\Bitm\SEIP107268\Utility\Utility;

        $book = new BookTitle();

        $books = $book->show($_GET['id']);

        //Utility::redirect();
        ?>
        <div class="container">

            <h3>Edit existing Book</h3>
            <form class="form-inline form-group " action="update.php" method="POST">
                    <input 
                        type="hidden" 
                        class="form-control" 
                        name ="id"
                        value="<?php echo $books['ID'] ?>"
                        />
                    <label for="exampleInputName2">Update Book Name: </label>
                    
                    <input 
                        type="text" 
                        class="form-control" 
                        name ="title"
                        value="<?php echo $books['title'] ?>"
                        />


                <button type="submit" class="btn btn-default">Update</button>
            </form>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>