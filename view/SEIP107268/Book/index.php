<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Book Title</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'atomicproject-rahad-107268' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

        use \App\Bitm\SEIP107268\Book\BookTitle;
        use App\Bitm\SEIP107268\Utility\Utility;

$book = new BookTitle();
        $books = $book->index();
        ?>

        <div class="container">

            <h3 class="text-center text-success">Book List</h3>

            <div id="msg" style="background-color: #46b8da; color: #F00; font-size: 25px;">
                <?php echo Utility::message(); ?>            
            </div>
            <hr />
            <a href="create.php" class="btn btn-success ">Add new Book</a>
            <hr>
            <table class="table table-bordered table-hover text-center bg-info">
                <thead >
                    <tr>
                        <th class="text-center">Sr No.</th>
                        <th class="text-center">ID</th>
                        <th class="text-center">Book name</th>
                        <th colspan="3" class="text-center">Action</th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sr = 1;
                    foreach ($books as $bk) {
                        ?>
                        <tr>
                            <td><?php echo $sr ?></td>
                            <td><?php echo $bk->ID ?></td>
                            <td><?php echo $bk->title;
                    $sr++;
                        ?></td>
                            <td><a href="show.php?id=<?php echo $bk->ID ?>" class="btn btn-success ">View</a></td>
                            <td><a href="edit.php?id=<?php echo $bk->ID ?>" class="btn btn-primary ">Edit</a></td>
                            <td>
                                <form action="delete.php" method="POST">
                                    <input type="hidden" name="id" value="<?php echo $bk->ID; ?>"/>
                                    <button class="btn btn-danger delete" type="submit" >Delete</button>
                                </form>
                            </td>
                        </tr>
                    <?php }
                    ?>
                </tbody>
            </table>

            <a href="" class="pull-left "><button class="btn btn-danger" onclick="window.history.go(-1)">Back</button></a>
            <button class="btn btn-danger pull-right"><a href="./../../../index.php"  class="pull-left ">Back to Project List</a></button>

            <nav class="text-center">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
        <script>
            $('.delete').bind('click',function(e){
                var dlt = confirm("Are you sure to Delete book Title");
                if(!dlt){
                    e.preventDefault();
                }
            });
            $('#msg').fadeOut(5000);
        </script>
        
        
    </body>
</html>