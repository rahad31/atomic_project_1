<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Subscription</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
        
        include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'atomicproject-rahad-107268'.DIRECTORY_SEPARATOR."view".DIRECTORY_SEPARATOR."startup.php");


        use App\Bitm\SEIP107268\Newsletter\Subscription;
        use App\Bitm\SEIP107268\Utility\Utility;

        $email = new Subscription();

       $emails = $email->show($_GET['id']);

        //Utility::redirect();
        ?>
         <div class="container">

            <h3 class="text-center">View Email</h3>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><?php echo $emails['id'] ?></td>
                        <td><?php echo $emails['email'] ?></td>
                    </tr>
                </tbody>
            </table>
            <nav><a href="index.php">Go to Home</a></nav>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
    </body>
</html>