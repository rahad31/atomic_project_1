<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Pofile Picture</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'atomicproject-rahad-107268' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

        use App\Bitm\SEIP107268\Profilepicture\Picture;
        use App\Bitm\SEIP107268\Utility\Utility;

        $pp = new Picture();
        $pps = $pp->show($_GET['id']);
        ?>
         <div class="container">
             <h3 class="text-info">Update Picture</h3>
          <form class="form-inline" role="form" action="update.php" method="post" enctype="multipart/form-data">
              <hr/>  
              <div class="form-group">
                  <label class="control-label col-sm-3" for="field1">Name:</label>
                  <div class="col-sm-9">
                      <input class="form-control" type="hidden" name="id" value="<?php echo $pps->id;?>">
                    <input type="text" name="name" class="form-control" id="field1" value="<?php echo $pps->name;?>">
                  </div>
                </div>
              <hr/>
              <div class=" ">
                  <div class="img-thumbnail ">
                      <h3 class="text-info">Existing Picture</h3>
                      <img src="upload/<?php echo $pps->profile_pic;?>"/>
                  </div>
                  
              </div>
              <hr/>
                <div class="form-group">
                  <label class="control-label col-sm-4" for="field2">New Profile Picture:</label>
                  <div class="col-sm-8">          
                    <input class="form-control" type="file" name="profile_pic" class="form-control" id="field2"  value="<?php echo $pps->profile_pic;?>">
                  </div>
                </div>
              <hr/>
                <div class="form-group">        
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-success" name="submit">Update</button>
                  </div>
                </div>
              </form>
          <p class="text-center"><a href="../../index.php">Go to Homepage</a> | <a href="index.php">Profile Picture List</a></p>
      </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>