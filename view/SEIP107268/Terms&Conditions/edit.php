<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Terms & Conditions</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'atomicproject-rahad-107268' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

        use App\Bitm\SEIP107268\Terms\TermsConditions;
        use App\Bitm\SEIP107268\Utility\Utility;

        $terms = new TermsConditions();
        $term =  $terms->show($_GET['id']);
        ?>
        
        
        <div class="container">

            <h3>Terms & Conditions List</h3>
            <form class="form-inline" action="update.php" method="post">
                <div class="form-group">
                    
                    <input type="hidden" name="id" value="<?php echo $term['id'] ?>" />
                    <label for="exampleInputName2">Enter Name: </label>
                    <textarea class="form-control" type="text" name="detail" placeholder="<?php echo $term['terms_details'] ?>" ></textarea>
                    <br>
                    <br>
                    <br>
                    <label class="checkbox-inline">
                        <input type="radio" name="terms"  value="Agree" /> Agree
                    </label>
                    <label class="checkbox-inline">
                        <input type="radio" name="terms"  value="Decline" /> Decline
                    </label>



                </div>


                <button type="submit" class="btn btn-success btn-md" >Save</button>
                
            </form>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
        
    </body>
</html>