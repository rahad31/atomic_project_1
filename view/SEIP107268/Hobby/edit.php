<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Hobby</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <?php
        include_once($_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . 'atomicproject-rahad-107268' . DIRECTORY_SEPARATOR . "view" . DIRECTORY_SEPARATOR . "startup.php");

        use App\Bitm\SEIP107268\Hobby\HobbyName;
        use App\Bitm\SEIP107268\Utility\Utility;

        $game = new HobbyName();
        $games = $game->show($_GET['id']);
        ?>

        <div class="container">

            <h3>Update Hobby</h3>
            <form class="form-inline" action="update.php" method="POST">
                
                <input 
                        type="hidden" 
                        class="form-control" 
                        name ="id"
                        value="<?php echo $games['id'] ?>"
                        />
                
                <div class="form-group">
                    <label for="exampleInputName2">Select hobby: </label>
                    <?php
                    
                        $var = explode(',', trim($games['game_name']));
                        
                    ?>
                    <input type="checkbox" name="games[]"  value="Football" <?php if(in_array("Football",$var)){ echo " checked";} ?>><label>Football</label><br>
                    <input type="checkbox" name="games[]"  value="Basket Ball" <?php if(in_array("Basket Ball",$var)){ echo " checked";} ?>><label>Basket Ball</label><br>
                    <input type="checkbox" name="games[]"  value="Pool" <?php if(in_array("Pool",$var)){ echo " checked";} ?>><label>Pool</label><br>
                    <input type="checkbox" name="games[]"  value="Rugby" <?php if(in_array("Rugby",$var)){ echo " checked";} ?>><label>Rugby</label><br>
                    <input type="checkbox" name="games[]"  value="Tennis" <?php if(in_array("Tennis",$var)){ echo " checked";} ?>><label>Tennis</label><br>
                    <input type="checkbox" name="games[]"  value="Cricket" <?php if(in_array("Cricket",$var)){ echo " checked";} ?>><label>Cricket</label><br>
                    <input type="checkbox" name="games[]"  value="Table Tennis" <?php if(in_array("Table Tennis",$var)){ echo " checked";} ?>><label>Table Tennis</label><br>
                    <input type="checkbox" name="games[]"  value="Hockey"  <?php if(in_array("Hockey",$var)){ echo " checked";} ?>><label>Hockey</label><br>

                </div>
               
                <hr>
                <button type="submit" class="btn btn-default">Update</button>
            </form>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>
         <script>
            
        </script>
    </body>
</html>