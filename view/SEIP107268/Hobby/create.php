<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Hobby</title>

        <!-- Bootstrap -->
        <link href="../../../asset/css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-info">
        <div class="container">
            <div class="row">
            <h3 class="text-center text-success">Add New Hobby</h3>
            <hr>
            <form class="form-inline" action="store.php" method="POST">
                <div class="form-group text-primary">
                    <label for="exampleInputName2">Select hobby: </label>

                </div>
                
                    <input type="checkbox" name="games[]" value="Football" ><label>Football</label><br>
                    <input type="checkbox" name="games[]" value="Basket Ball" ><label>Basket Ball</label><br>
                    <input type="checkbox" name="games[]" value="Pool" ><label>Pool</label><br>
                    <input type="checkbox" name="games[]" value="Rugby" ><label>Rugby</label><br>
                    <input type="checkbox" name="games[]" value="Tennis" ><label>Tennis</label><br>
                    <input type="checkbox" name="games[]" value="Cricket"><label>Cricket</label><br>
                    <input type="checkbox" name="games[]" value="Table Tennis"><label>Table Tennis</label><br>
                    <input type="checkbox" name="games[]" value="Hockey" ><label>Hockey</label><br>

                <hr>
                <button type="submit" name="submit" class="btn btn-success">Create</button>
            </form>
            </div><!-- /.row -->


        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="../../../asset/js/bootstrap.min.js"></script>

    </body>
</html>