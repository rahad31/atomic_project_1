<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Atomic Project | BITM</title>

        <!-- Bootstrap -->
        <link href="asset/css/bootstrap.css" rel="stylesheet">
        <link href="asset/css/bootstrap-theme.min.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-info">
        <div class="container">
            
            <fieldset>
                <legend class="text-center text-danger h2">Web App Development with PHP</legend>
                <h3 class="text-center text-success">Group Name: Debug & Die</h3>
            <h4 class="text-center">Md. Rahaduzzaman ID: 107268, Md. Mazharul Islam (107369), Md. Shibly Nomaan (106733), Afroja Khatun (106143) </h4>
            <h4 class="text-center text-danger">Batch: 11 </h4>
            </fieldset>
            

            <hr>
            <table class="table table-bordered table-hover text-center bg-success">
                <thead >
                    <tr>
                        <th class="text-center">Serial</th>
                        <th class="text-center">Page link</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1.</td>
                        <td><a href="view/SEIP107268/Birthday/" class="text-success ">Birthday</a></td>
                        
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td><a href="view/SEIP107268/Book/" class="text-success ">Book</a></td>
                        
                    </tr>
                    <tr>
                         <td>3.</td>
                         <td><a href="view/SEIP107268/City/" class="text-success ">City</a></td>
                        
                    </tr>
                    <tr>
                         <td>4.</td>
                         <td><a href="view/SEIP107268/Educationlevel/" class="text-success ">Education level</a></td>
                        
                    </tr>
                    <tr>
                         <td>5.</td>
                         <td><a href="view/SEIP107268/Hobby/" class="text-success ">Hobby</a></td>
                        
                    </tr>
                    <tr>
                         <td>6.</td>
                         <td><a href="view/SEIP107268/Newsletter/" class="text-success ">Subscription</a></td>
                        
                    </tr>
                    <tr>
                         <td>7.</td>
                         <td><a href="view/SEIP107268/Profilepicture/" class="text-success ">Profile Picture</a></td>
                        
                    </tr>
                    <tr>
                         <td>8.</td>
                         <td><a href="view/SEIP107268/Summary/" class="text-success ">Summary</a></td>
                        
                    </tr>
                    <tr>
                         <td>9.</td>
                         <td><a href="view/SEIP107268/Terms&Conditions/" class="text-success ">Terms&Conditions</a></td>
                        
                    </tr>
                </tbody>
            </table>

        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html><!DOCTYPE html>

